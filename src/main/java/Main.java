import com.mashape.unirest.http.exceptions.UnirestException;
import demo.HttpDemo;

public class Main {
    public static void main(String[] args) {
        try {
            new HttpDemo().execute();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
}
