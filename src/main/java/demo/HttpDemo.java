package demo;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.HashMap;
import java.util.Map;

public class HttpDemo {
    public void execute() throws UnirestException {
        System.out.println("---------------------- GET ----------------------");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("pagesize", 1);
        parameters.put("todate", 1548633600);
        parameters.put("order", "desc");
        parameters.put("max", 1548720000);
        parameters.put("sort", "activity");
        parameters.put("site", "stackoverflow");
        HttpResponse<JsonNode> response = Unirest.get("https://api.stackexchange.com/2.2/questions")
                .header("accept", "application/json")
                .queryString(parameters)
                .asJson();
        printResponse(response);

        System.out.println("---------------------- GET ----------------------");
        response = Unirest.get("http://www.mocky.io/v2/5a9ce37b3100004f00ab5154")
                .header("accept", "application/json")
                .queryString("hello", "world")
                .asJson();
        printResponse(response);

        System.out.println("---------------------- POST ----------------------");
        response = Unirest.post("http://httpbin.org/post")
                .header("accept", "application/json")
                .body("{\"param0\":\"value0\", \"param1\":\"value1\"}")
                .asJson();
        printResponse(response);

        System.out.println("---------------------- POST ----------------------");
        response = Unirest.post("http://www.mocky.io/v2/5a9ce7663100006800ab515d")
                .header("accept", "application/json")
                .body("{\"name\":\"Homer\", \"city\":\"Springfield\"}")
                .asJson();
        printResponse(response);
    }

    private void printResponse(HttpResponse<JsonNode> response) {
        System.out.println("Status: " + response.getStatus() + " " +
                response.getStatusText() +
                "\nHeaders: " + response.getHeaders() +
                "\nBody: " + response.getBody());
    }
}
